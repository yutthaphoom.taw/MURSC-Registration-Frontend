import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
    enableProdMode();
}

var isBrowserSupport = true;
var userAgent = navigator.userAgent;
var platform = navigator.platform;
var isEdge = (userAgent.indexOf('Edge') > -1);
var isIE = ((/MSIE|Trident/).test(userAgent) && !isEdge);
var isSafari = (/^((?!chrome|android).)*safari/i.test(userAgent));
var isOpera = ((userAgent.indexOf("Opera") > -1) || (userAgent.indexOf('OPR') > -1));
var isWindow = (platform.indexOf('Win') > -1)
var isTablet = false;
var isMobile = false;

if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(userAgent))
    isTablet = true;

if (/Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(userAgent))
    isMobile = true;

if (isIE ||
    (isSafari && isWindow)) {
    isBrowserSupport = false;

    if (isSafari &&
        isWindow &&
        (isTablet || isMobile))
        isBrowserSupport = true;
}

if (isTablet ||
    isMobile)
    document.getElementsByTagName('body')[0].classList.add('mobile');

if (isOpera)
    document.getElementsByTagName('body')[0].classList.add('opera');

if (!isBrowserSupport) {
    document.title = 'Unsupported Browser';

    document.getElementById('header-title').innerHTML = "Unsupported browser";
    document.getElementById('body-content').innerHTML = "You are using a browser that is not supported.<br/>This web app works on the latest versions of <strong>Google Chrome</strong>, <strong>Mozilla Firefox</strong>, <strong>Apple Safari</strong>, or <strong>Microsoft Edge</strong>.";
    document.getElementById('unsupported-page').classList.remove('hidden');
}
else {
    platformBrowserDynamic().bootstrapModule(AppModule)
        .catch(err => console.error(err));
}
