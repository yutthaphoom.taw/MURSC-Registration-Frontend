/*
=============================================
Author      : <ยุทธภูมิ ตวันนา>
Create date : <๒๒/๐๒/๒๕๖๓>
Modify date : <๐๙/๐๖/๒๕๖๕>
Description : <>
=============================================
*/

'use strict';

import { Injectable } from '@angular/core';

import { AppService } from './app.service';

export namespace Schema {
    export interface Country {
        ID?: string,
        name?: {
            th?: string,
            en?: string
        },
        isoCountryCodes2Letter?: string,
        isoCountryCodes3Letter?: string
    }

    export interface Province {
        ID?: string,
        countryID?: string,
        isoCountryCodes3Letter?: string,
        name?: {
            th?: string,
            en?: string
        },
        regional?: string
    }

    export interface District {
        ID?: string,
        countryID?: string,
        isoCountryCodes3Letter?: string,
        provinceID?: string,
        provinceName?: {
            th?: string,
            en?: string
        },
        name?: {
            th?: string,
            en?: string
        },
        zipCode?: string
    }

    export interface Subdistrict {
        ID?: string,
        countryID?: string,
        isoCountryCodes3Letter?: string,
        provinceID?: string,
        provinceName?: {
            th?: string,
            en?: string
        },
        districtID?: string,
        districtName?: {
            th?: string,
            en?: string
        },
        zipCode?: string,
        name?: {
            th?: string,
            en?: string
        }
    }

    export interface ProjectCategory {
        ID?: string,
        sequence?: number,
        name?: {
            th?: string,
            en?: string
        },
        logo?: string,
        initial?: string,
        projectCount?: number
    }

    export interface Project {
        ID?: string,
        category?: ProjectCategory,
        type?: string,
        name?: {
            th?: string,
            en?: string,
        },
        about?: {
            th?: string,
            en?: string
        },
        ownerCode?: string,
        minimumPassScore?: number,
        logo?: string,
        subject?: string,
        isExam?: string,
        isTeaching?: string
    }

    export interface ContactPerson {
        fullName?: {
            th?: string,
            en?: string
        },
        email?: string,
        phoneNumber?: string
    }

    export interface Faculty {
        code?: string;
        name?: {
            th?: string,
            en?: string
        }
    }

    export interface Building {
        ID?: string,
        name?: {
            th?: string,
            en?: string
        },
        faculty?: Faculty
    }

    export interface Location {
        ID?: string,
        building?: Building,
        name?: {
            th?: string,
            en?: string,
        },
        seatMaximum?: number
    }

    export interface FeeType {
        ID?: string,
        name?: {
            th?: string,
            en?: string
        };
        amount?: number,
        toggle?: string
    }

    export interface Invoice {
        ID?: string,
        transRegisteredID?: string,
        name?: {
            th?: string,
            en?: string
        };
        namePrintReceipt?: string,
        billerID?: string,
        merchantName?: string,
        qrRef1?: string,
        qrRef2?: string,
        qrRef3?: string,
        qrImage?: string,
        qrNewRef1?: string,
        bankRequest?: string,
        bankTransID?: string,
        payment?: {
            amount?: number,
            confirmDate?: string,
            by?: string,
            date?: string,
            status?: string
        },
        privilege?: Privilege
    }

    export interface InvoiceFee {
        invoiceID?: string,
        feeType?: FeeType
    }

    export interface TransProject {
        ID?: string,
        CUID?: string,
        project?: Project,
        description?: {
            th?: string,
            en?: string
        },
        examDate?: {
            startDate?: string,
            endDate?: string
        },
        registrationDate?: {
            startDate?: string,
            endDate?: string
        },
        lastPaymentDate?: string,
        paymentExpire?: string,
        announceDate?: string,
        maximumSeat?: number,
        seatReserved?: number,
        minimumFee?: string,
        contactPerson?: ContactPerson[],
        registrationStatus?: string,
        userTypeSpecific?: [],
        transLocation?: TransLocation[],
        transFeeType?: TransFeeType[]
    }

    export interface TransLocation {
        ID?: string,
        transProjectID?: string,
        location?: Location,
        seatTotal?: number,
        seatAvailable?: number,
    }

    export interface TransFeeType {
        ID?: string;
        transProjectID?: string,
        feeType?: FeeType;
        requiredStatus?: string,
        isSelected?: boolean
    }

    export interface TransDeliAddress {
        ID?: string,
        transRegisteredID?: string,
        address?: string,
        country?: Country,
        province?: Province,
        district?: District,
        subdistrict?: Subdistrict,
        postalCode?: string,
        phoneNumber?: string
    }

    export interface TransScore {
        transRegisteredID?: string,
        eventCode?: string,
        subject?: string,
        applicantNo?: string,
        adfsID?: string,
        totalScore?: number,
        examResult?: string
    }

    export interface TransRegistered {
        ID?: string,
        CUID?: string,
        registeredDate?: string,
        transProject?: TransProject,
        transLocation?: TransLocation,
        transDeliAddress?: TransDeliAddress,
        invoice?: Invoice,
        invoiceFee?: InvoiceFee[],
        totalFeeAmount?: number,
        transFeeType?: TransFeeType[]
        seatNO?: string,
        applicantNO?: string,
        transScore?: TransScore
    }

    export interface Statuses {
        ID?: string;
        name: {
            th?: string,
            en?: string
        }
    }

    export interface QRCode {
        errorCode?: number,
        qrCode?: string,
        qrMessage?: string,
        qrFormat?: string,
        qrImage64?: string,
        qrResponse?: string,
        qrNewRef1?: string
    }

    export interface Schedules {
        ID?: string,
        lessonName?: {
            th?: string,
            en?: string
        },
        dateTime?: {
            start?: {
                date?: string,
                time?: string
            },
            end?: {
                date?: string,
                time?: string
            }
        },
        instructor?: string,
        typeSchedule?: string
    }

    export interface TransSchedule {
        ID?: string,
        transProject?: TransProject,
        section?: string,
        schedules?: Schedules[]
    }

    export interface Privilege {
        ID?: string,
        promoCode?: string,
        name?: string,
        detail?: string,
        discount?: number,
        expiredDate?: string,
        usagedDate?: string,
        status?: string
    }
}

namespace Data {
    export class Country {
        constructor(private appService: AppService) {
        }

        private getDataSource(
            action: string,
            query?: string
        ): Promise<Array<Schema.Country>> {
            return this.appService.getDataSource('Country', action, query).then((result: Array<Schema.Country>) => {
                return result.sort((a, b) => (a.name.th < b.name.th ? -1 : Number(a.name.th > b.name.th)));
            });
        }

        getList(): Promise<Array<Schema.Country>> {
            return this.getDataSource('getlist').then((result: Array<Schema.Country>) => {
                return result;
            })
        }

        get(countryID: string): Promise<Schema.Country> {
            let query = [
                '',
                ('country=' + countryID)
            ].join('&');

            return this.getDataSource('get', query).then((result: Array<Schema.Country>) => {
                return result[0];
            });
        }
    }

    export class Province {
        constructor(private appService: AppService) {
        }

        private getDataSource(
            action: string,
            query?: string
        ): Promise<Array<Schema.Province>> {
            return this.appService.getDataSource('Province', action, query).then((result: Array<Schema.Province>) => {
                return result.sort((a, b) => (a.name.th < b.name.th ? -1 : Number(a.name.th > b.name.th)));
            });
        }

        getList(countryID: string): Promise<Array<Schema.Province>> {
            let query = [
                '',
                ('country=' + countryID)
            ].join('&');

            return this.getDataSource('getlist', query).then((result: Array<Schema.Province>) => {
                return result;
            })
        }

        get(
            countryID:
            string, provinceID: string
        ): Promise<Schema.Province> {
            let query = [
                '',
                ('country=' + countryID),
                ('province=' + provinceID)
            ].join('&');

            return this.getDataSource('get', query).then((result: Array<Schema.Province>) => {
                return result[0];
            });
        }
    }

    export class District {
        constructor(private appService: AppService) {
        }

        private getDataSource(
            action: string,
            query?: string
        ): Promise<Array<Schema.District>> {
            return this.appService.getDataSource('District', action, query).then((result: Array<Schema.District>) => {
                return result.sort((a, b) => (a.name.th < b.name.th ? -1 : Number(a.name.th > b.name.th)));
            });
        }

        getList(
            countryID: string,
            provinceID: string
        ): Promise<Array<Schema.District>> {
            let query = [
                '',
                ('country=' + countryID),
                ('province=' + provinceID)
            ].join('&');

            return this.getDataSource('getlist', query).then((result: Array<Schema.District>) => {
                return result;
            })
        }

        get(
            countryID: string,
            provinceID: string,
            districtID: string
        ): Promise<Schema.District> {
            let query = [
                '',
                ('country=' + countryID),
                ('province=' + provinceID),
                ('district=' + districtID)
            ].join('&');

            return this.getDataSource('get', query).then((result: Array<Schema.District>) => {
                return result[0];
            });
        }
    }

    export class Subdistrict {
        constructor(private appService: AppService) {
        }

        private getDataSource(
            action: string,
            query?: string
        ): Promise<Array<Schema.Subdistrict>> {
            return this.appService.getDataSource('Subdistrict', action, query).then((result: Array<Schema.Subdistrict>) => {
                return result.sort((a, b) => (a.name.th < b.name.th ? -1 : Number(a.name.th > b.name.th)));
            });
        }

        getList(
            countryID: string,
            provinceID: string,
            districtID: string
        ): Promise<Array<Schema.Subdistrict>> {
            let query = [
                '',
                ('country=' + countryID),
                ('province=' + provinceID),
                ('district=' + districtID)
            ].join('&');

            return this.getDataSource('getlist', query).then((result: Array<Schema.Subdistrict>) => {
                return result;
            })
        }

        get(
            countryID: string,
            provinceID: string,
            districtID: string,
            subdistrictID: string
        ): Promise<Schema.Subdistrict> {
            let query = [
                '',
                ('country=' + countryID),
                ('province=' + provinceID),
                ('district=' + districtID),
                ('subdistrict=' + subdistrictID)
            ].join('&');

            return this.getDataSource('get', query).then((result: Array<Schema.Subdistrict>) => {
                return result[0];
            });
        }
    }

    export class ProjectCategory {
        constructor(private appService: AppService) {
        }

        private getDataSource(
            action: string,
            query?: string
        ): Promise<Array<Schema.ProjectCategory>> {
            return this.appService.getDataSource('ProjectCategory', action, query).then((result: Array<Schema.ProjectCategory>) => {
                return result;
            });
        }

        getList(): Promise<Array<Schema.ProjectCategory>> {
            return this.getDataSource('getlist').then((result: Array<Schema.ProjectCategory>) => {
                return result;
            })
        }

        get(projectCategory: string): Promise<Schema.ProjectCategory> {
            let query = [
                '',
                ('projectCategory=' + projectCategory)
            ].join('&');

            return this.getDataSource('get', query).then((result: Array<Schema.ProjectCategory>) => {
                return result[0];
            });
        }
    }

    export class TransProject {
        constructor(private appService: AppService) {
        }

        private getDataSource(
            action: string,
            query?: string,
            remark?: string
        ): Promise<Array<Schema.TransProject>> {
            return this.appService.getDataSource('TransProject', action, query, remark).then((result: Array<Schema.TransProject>) => {
                return result;
            });
        }

        getList(projectCategory: string): Promise<Array<Schema.TransProject>> {
            let query = [
                '',
                ('projectCategory=' + projectCategory)
            ].join('&');

            return this.getDataSource('getlist', query).then((result: Array<Schema.TransProject>) => {
                return result;
            })
        }

        get(
            projectCategory: string,
            cuid: string,
            remark?: string
        ): Promise<Schema.TransProject> {
            let query = [
                '',
                ('projectCategory=' + projectCategory),
                ('cuid=' + cuid)
            ].join('&');

            return this.getDataSource('get', query, remark).then((result: Array<Schema.TransProject>) => {
                return result[0];
            });
        }
    }

    export class TransRegistered {
        constructor(private appService: AppService) {
        }

        private getDataSource(
            action: string,
            query?: string
        ): Promise<Schema.TransRegistered[]> {
            return this.appService.getDataSource('TransRegistered', action, query).then((result: Array<Schema.TransRegistered>) => {
                return result;
            });
        }

        getList(paymentStatus: string): Promise<Array<Schema.TransRegistered>> {
            let query = [
                '',
                ('paymentStatus=' + paymentStatus)
            ].join('&');

            return this.getDataSource('getlist', query).then((result: Array<Schema.TransRegistered>) => {
                return result;
            })
        }

        get(cuid: string): Promise<Schema.TransRegistered> {
            let query = [
                '',
                ('cuid=' + cuid)
            ].join('&');

            return this.getDataSource('get', query).then((result: Array<Schema.TransRegistered>) => {
                return result[0];
            });
        }
    }

    export class Statuses {
        private data$: Array<Schema.Statuses> = [];

        private getDataSource(
            action: string,
            query?: string
        ): Array<Schema.Statuses> {
            let items: Array<Schema.Statuses>;

            if (action === 'getlist')
                items = this.data$;

            if (action === 'get')
                items = this.data$.filter(result => {
                    return result.ID.includes(query);
                });

            return items;
        }

        getList(data$: Array<Schema.Statuses>): Array<Schema.Statuses> {
            this.data$ = data$;

            return this.getDataSource('getlist')
        }

        get(
            data$: Array<Schema.Statuses>,
            query: string
        ): Schema.Statuses {
            this.data$ = data$;

            return this.getDataSource('get', query)[0];
        }
    }

    export class RegistrationStatus {
        public data$ = [
            {
                ID: 'Y',
                name: {
                    th: 'เปิดให้ลงทะเบียน',
                    en: 'Registration is open'
                }
            },
            {
                ID: 'W',
                name: {
                    th: 'ยังไม่เปิดให้ลงทะเบียน',
                    en: 'Registration not yet opened'
                }
            },
            {
                ID: 'N',
                name: {
                    th: 'หมดเวลาลงทะเบียน',
                    en: 'Registration expired'
                }
            }
        ]
    }

    export class PaymentStatus {
        public data$ = [
            {
                ID: 'Y',
                name: {
                    th: 'ชำระเงินเรียบร้อย',
                    en: 'Payment completed'
                }
            },
            {
                ID: 'W',
                name: {
                    th: 'ตรวจสอบการชำระเงิน',
                    en: 'Check payment'
                }
            },
            {
                ID: 'N',
                name: {
                    th: 'รอการชำระเงิน',
                    en: 'Pending payment'
                }
            }
        ]
    }

    export class QRCode {
        constructor(private appService: AppService) {
        }

        private getDataSource(
            action: string,
            query?: string
        ): Promise<Array<Schema.QRCode>> {
            return this.appService.getDataSource('QRCodePayment', action, query).then((result: Array<any>) => {
                let qrcode: Array<Schema.QRCode> = [];

                for (let dr of result) {
                    qrcode.push({
                        errorCode: dr['errorCode'],
                        qrCode: (dr['qrCode'] ? dr['qrCode'] : ''),
                        qrMessage: (dr['qrMessage'] ? dr['qrMessage'] : ''),
                        qrFormat: (dr['qrFormat'] ? dr['qrFormat'] : ''),
                        qrImage64: (dr['qrImage64'] ? dr['qrImage64'] : ''),
                        qrResponse: (dr['qrResponse'] ? dr['qrResponse'] : ''),
                        qrNewRef1: (dr['qrNewRef1'] ? dr['qrNewRef1'] : '')
                    });
                }

                return qrcode;
            });
        }

        get(cuid: string): Promise<Schema.QRCode> {
            let query = [
                '',
                ('cuid=' + cuid)
            ].join('&');

            return this.getDataSource('get', query).then((result: Array<Schema.QRCode>) => {
                return result[0];
            });
        }
    }

    export class TransSchedule {
        constructor(private appService: AppService) {
        }

        private getDataSource(
            action: string,
            query?: string
        ): Promise<Array<Schema.TransSchedule>> {
            return this.appService.getDataSource('TransSchedule', action, query).then((result: Array<Schema.TransSchedule>) => {
                return result;
            });
        }

        get(
            projectCategory: string,
            cuid: string
        ): Promise<Array<Schema.TransSchedule>> {
            let query = [
                '',
                ('projectCategory=' + projectCategory),
                ('cuid=' + cuid)
            ].join('&');

            return this.getDataSource('get', query).then((result: Array<Schema.TransSchedule>) => {
                return result;
            });
        }
    }

    export class MaskPhoneNumber {
        public data$ = [
            {
                ID: 'mobile',
                type: {
                    name: {
                        th: 'โทรศัพท์มือถือ',
                        en: 'Mobile phone'
                     },
                     image: '/assets/images/smartphone.png',
                },
                mask: '99 9999 9999'
            },
            {
                ID: 'home',
                type: {
                    name: {
                        th: 'โทรศัพท์บ้าน',
                        en: 'Mobile phone'
                    },
                    image: '/assets/images/phone.png',
                },
                mask: '9 9999 9999'
            }
        ]
    }
}

@Injectable({
    providedIn: 'root'
})
export class DataService {
    constructor(private appService: AppService) {
    }

    public country = new Data.Country(this.appService);
    public province = new Data.Province(this.appService);
    public district = new Data.District(this.appService);
    public subdistrict = new Data.Subdistrict(this.appService);
    public projectCategory = new Data.ProjectCategory(this.appService);
    public transProject = new Data.TransProject(this.appService);
    public transRegistered = new Data.TransRegistered(this.appService);
    public statuses = new Data.Statuses();
    public registrationStatus = new Data.RegistrationStatus();
    public paymentStatus = new Data.PaymentStatus();
    public qrcode = new Data.QRCode(this.appService);
    public transSchedule = new Data.TransSchedule(this.appService);
    public maskPhoneNumber = new Data.MaskPhoneNumber();
}
