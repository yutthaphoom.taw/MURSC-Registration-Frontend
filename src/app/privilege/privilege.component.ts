/*
=============================================
Author      : <ยุทธภูมิ ตวันนา>
Create date : <๒๓/๑๑/๒๕๖๓>
Modify date : <๐๘/๐๖/๒๕๖๕>
Description : <>
=============================================
*/

'use strict';

import { Component, Input, OnInit } from '@angular/core';

import { AppService } from '../app.service';
import { Schema } from '../data.service';

@Component({
    selector: 'app-privilege',
    templateUrl: './privilege.component.html',
    styleUrls: ['./privilege.component.scss']
})
export class PrivilegeComponent implements OnInit {
    @Input() data$: Schema.Privilege;

    constructor(private appService: AppService) {
    }

    data: any = {
        privilege$: null
    };

    ngOnInit(): void {
        this.data.privilege$ = this.data$;
    }
}
