/*
=============================================
Author      : <ยุทธภูมิ ตวันนา>
Create date : <๑๑/๐๘/๒๕๖๓>
Modify date : <๑๒/๐๘/๒๕๖๓>
Description : <>
=============================================
*/

'use strict';

import { Injectable, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { debounceTime, delay, switchMap, tap } from 'rxjs/operators';

import { Schema, DataService } from '../../data.service';

interface TableState {
    page: number;
    pageSize: number;
    keyword: string;
    projectCategory: string;
}

interface TableSearchResult {
    data: Array<Schema.TransRegistered>;
    total: number;
    totalSearch: number;
}

class Table {
    constructor(
        private pipe: DecimalPipe,
        private dataService: DataService,
        private paymentStatus: string
    ) {
        this.reload(this.paymentStatus);
    }

    private _searching$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
    private _search$: Subject<void> = new Subject<void>();
    private _data$: BehaviorSubject<Array<Schema.TransRegistered>> = new BehaviorSubject<Array<Schema.TransRegistered>>([]);
    private _total$: BehaviorSubject<number> = new BehaviorSubject<number>(0);
    private _totalSearch$: BehaviorSubject<number> = new BehaviorSubject<number>(0);

    private _state: TableState = {
        page: 1,
        pageSize: 4,
        keyword: null,
        projectCategory: null
    };

    get data$() {
        return this._data$.asObservable();
    }

    get total$() {
        return this._total$.asObservable();
    }

    get totalSearch$() {
        return this._totalSearch$.asObservable();
    }

    get searching$() {
        return this._searching$.asObservable();
    }

    get page() {
        return this._state.page;
    }

    get pageSize() {
        return this._state.pageSize;
    }

    get keyword() {
        return this._state.keyword;
    }

    get projectCategory() {
        return this._state.projectCategory;
    }

    set page(page: number) {
        this._set({ page });
    }

    set pageSize(pageSize: number) {
        this._set({ pageSize });
    }

    set keyword(keyword: string) {
        this._set({ keyword });
    }

    set projectCategory(projectCategory: string) {
        this._set({ projectCategory });
    }

    private _set(patch: Partial<TableState>): void {
        Object.assign(this._state, patch);
        this._search$.next();
    }

    private _search(data: Array<Schema.TransRegistered>): Observable<TableSearchResult> {
        const { page, pageSize, keyword, projectCategory } = this._state;

        let tmp: Array<Schema.TransRegistered> = data;

        tmp = tmp.filter(registered => this.matches(registered, keyword, projectCategory, this.pipe));
        const total: number = data.length;
        const totalSearch: number = tmp.length;

        data = tmp;

        return of({ data, total, totalSearch });
    }

    matches(
        data: Schema.TransRegistered,
        keyword: string,
        projectCategory: string,
        pipe: PipeTransform
    ): boolean {
        keyword = (keyword ? keyword : '');
        projectCategory = (projectCategory ? projectCategory : '');

        return (
            (
                data.transProject.project.name.th.toLowerCase().includes(keyword.toLowerCase()) ||
                data.transProject.project.name.en.toLowerCase().includes(keyword.toLowerCase()) ||
                (data.transProject.description.th !== null ? data.transProject.description.th.toLowerCase().includes(keyword.toLowerCase()) : '') ||
                (data.transProject.description.en !== null ? data.transProject.description.en.toLowerCase().includes(keyword.toLowerCase()) : '')
            ) &&
            (
                data.transProject.project.category.ID.includes(projectCategory) ||
                data.transProject.project.category.initial.includes(projectCategory)
            )
        )
    }

    reload(paymentStatus: string): void {
        this.dataService.transRegistered.getList(paymentStatus).then((result: Array<Schema.TransRegistered>) => {
            this._search$.pipe(
                tap(() => this._searching$.next(true)),
                debounceTime(100),
                switchMap(() => this._search(result)),
                delay(100),
                tap(() => this._searching$.next(false))
            ).subscribe(result => {
                this._data$.next(result.data);
                this._total$.next(result.total);
                this._totalSearch$.next(result.totalSearch);
            });

            this._search$.next();
        });
    }
}

class Operate {
    constructor(
        private pipe: DecimalPipe,
        private dataService: DataService,
        private paymentStatus: string
    ) {
    }

    table = {
        service: new Table(this.pipe, this.dataService, this.paymentStatus),
        filter: {
            showForm: false,
            setValue(): void {
                this.showForm = false;
            }
        }
    };
}

@Injectable({
    providedIn: 'root'
})
export class TransactionRegisteredService {
    constructor() {
    }

    public service: any = null;
}

@Injectable({
    providedIn: 'root'
})
export class TransactionRegisteredAllService {
    constructor(
        private pipe: DecimalPipe,
        private dataService: DataService
    ) {
    }

    public paymentStatus: string = '';
    public operate: Operate = new Operate(this.pipe, this.dataService, this.paymentStatus);
}

@Injectable({
    providedIn: 'root'
})
export class TransactionRegisteredPaymentCompletedService {
    constructor(
        private pipe: DecimalPipe,
        private dataService: DataService
    ) {
    }

    public paymentStatus: string = 'Y'
    public operate: Operate = new Operate(this.pipe, this.dataService, this.paymentStatus);
}

@Injectable({
    providedIn: 'root'
})
export class TransactionRegisteredCheckPaymentService {
    constructor(
        private pipe: DecimalPipe,
        private dataService: DataService
    ) {
    }

    public paymentStatus: string = 'W';
    public operate: Operate = new Operate(this.pipe, this.dataService, this.paymentStatus);
}

@Injectable({
    providedIn: 'root'
})
export class TransactionRegisteredPendingPaymentService {
    constructor(
        private pipe: DecimalPipe,
        private dataService: DataService
    ) {
    }

    public paymentStatus: string = 'N';
    public operate: Operate = new Operate(this.pipe, this.dataService, this.paymentStatus);
}
